json.extract! admin_invoice, :id, :valor, :description, :seller_id, :created_at, :updated_at
json.url admin_invoice_url(admin_invoice, format: :json)

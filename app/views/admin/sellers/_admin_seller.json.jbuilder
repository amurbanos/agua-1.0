json.extract! admin_seller, :id, :nome, :telefone, :documento, :obs, :ativo, :imei, :created_at, :updated_at
json.url admin_seller_url(admin_seller, format: :json)

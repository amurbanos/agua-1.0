json.extract! admin_cliente, :id, :nome, :telefone, :latitude, :longitude, :endereco, :obs, :created_at, :updated_at
json.url admin_cliente_url(admin_cliente, format: :json)

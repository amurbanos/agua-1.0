json.extract! admin_delivery, :id, :valor, :latitude, :longitude, :obs, :seller_id, :client_id, :created_at, :updated_at
json.url admin_delivery_url(admin_delivery, format: :json)

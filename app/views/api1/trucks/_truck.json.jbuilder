
# Caminhao
json.merge! truck.attributes   



# produtos no caminhao 
if @seller_id
	
	product_seller = ProductSeller.select( :product_id )
	    .where(:seller_id   => @seller_id)  

	truck_product_seller = 
	    TruckProduct
	        .where(:truck_id   => truck.id)
		    .where("product_id IN (?)", product_seller)  

	json.truck_products truck_product_seller, partial: 'truck_product', as: :truck_product

else

    json.truck_products truck.truck_product, partial: 'truck_product', as: :truck_product

end







# garrafao
json.bottles truck.bottle, partial: '/api1/bottles/bottle', as: :bottle  
json.bottles_count truck.bottle.sum( :quantidade )

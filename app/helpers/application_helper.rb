module ApplicationHelper


  def grana( valor )
    return number_with_precision( valor, :precision => 2)
  end


  def cpf_rg ( doc)

  	cpf = CPF.new(doc)
  	return cpf.formatted 

  end


  def date_br ( date )

	   return  date.strftime("%d/%m/%Y")   

  end

  def datetime_br ( date )

  	return  date.strftime("%d/%m/%Y - %H:%M")   

  end


end

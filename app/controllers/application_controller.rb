class ApplicationController < ActionController::Base

     before_action :set_general_site_data


	def after_sign_out_path_for(resource_or_scope)
	  request.referrer
	end



	def set_general_site_data

	 	@general_site_name  = "Mister Água" 
	 	@general_site_path  = "misteragua"   

    end


end

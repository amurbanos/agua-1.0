class Api1::SellersController < ApplicationController


    skip_before_action :verify_authenticity_token



	def set_truck

		puts "-----------------------------"


		@seller = Seller.find( params[:id] )
		@seller.truck_id = params[:truck][:id]
		@seller.save

		#
		truck_products = params[:truck][:truck_products]  


		truck_products.each do |truck_product|

			# Recupera cada produto
			tp_tmp = TruckProduct
			    .where( [ "truck_products.product_id  = ?", truck_product[:product_id] ] )  
			    .where( [ "truck_products.truck_id    = ?", truck_product[:truck_id] ] )
			    .first  

			qtd_anterior_caminhao      =  tp_tmp.quantidade_caminhao 

			if truck_product[:quantidade_caminhao] > 0   
				tp_tmp.quantidade_caminhao = truck_product[:quantidade_caminhao]
			end


			tp_tmp.quantidade_fonte =  
				tp_tmp.quantidade_fonte +  
				truck_product[:quantidade_caminhao] - 
				qtd_anterior_caminhao

			tp_tmp.save 

			# Adicionar como venda
			qtd_diferenca = tp_tmp.quantidade_caminhao - qtd_anterior_caminhao 

			if qtd_diferenca > 0    

				# Determina o preco de acordo com dia da semana
				preco_final = tp_tmp.product.preco_compra_1 * qtd_diferenca


				produto = Product.find( tp_tmp.product_id )


				puts "===================================="
				dia_compra =  params[:dataCompra].to_time 
				puts dia_compra.wday.inspect        
				puts "===================================="  


				if produto.brand.dia_desconto == dia_compra.wday    

  					preco_final = tp_tmp.product.preco_compra_2 * qtd_diferenca

				end


                # salva o produto
				buy = Buy.new  


				buy.quantidade  = qtd_diferenca

				buy.valor       = preco_final

				buy.get_at      = params[:dataCompra].to_time 

				buy.valor       = preco_final

				buy.truck_id    = tp_tmp.truck_id    

				buy.product_id  = tp_tmp.product_id  

				buy.seller_id   = @seller.id

				buy.save

			end

		end  

		# salva o ponto do dia
		Point.beat_today( params[:id], params[:assistant_id] )

        @status            = Hash.new       
        @status[:error]    = 0 

	end


    def set_location


    	if params[:seller_id] != nil

	    	seller = Seller.find( params[:seller_id] )

	    	location = Location.new
	    	
	    	location.seller_id  = seller.id
	    	location.truck_id   = seller.truck.id
	    	location.latitude   = params[:latitude]  
	    	location.longitude  = params[:longitude]

	    	location.save  

    	end

    end



	private

	    def truck_product_params
	      params.require(:truck).permit(:placa, :obs)
	    end


end
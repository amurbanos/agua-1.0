class Api1::DeliveriesController < ApplicationController

    skip_before_action :verify_authenticity_token

    def sign

        @seller = Seller.find_by(usuario: params[:usuario], senha: params[:senha])

        @status = Hash.new 

        if @seller 
            @status[:message] = 'Logado com sucesso'
            @status[:code]    = 1
        else
            @seller = Seller.new
            @status[:message] = 'Usuário não cadastrado'   
            @status[:code]    = 0            
        end

    end


    def get_by_seller

        # entregas
        data_inicial = params[:data_inicial].to_datetime.strftime("%Y-%m-%d 00:00:00")
        data_final   = params[:data_final].to_datetime.strftime("%Y-%m-%d 23:59:59")  

        @deliveries = Delivery
            .where( [ "deliveries.seller_id    = ?", params[:seller_id] ] )   


        if params[:status] == "100"

            @deliveries = @deliveries
                .where( [ "deliveries.created_at   > ?", data_inicial] )
                .where( [ "deliveries.created_at   < ?", data_final] ) 

        end


        if params[:status] == "1"

            @deliveries = @deliveries
                .where( [ "deliveries.created_at   > ?", data_inicial] )
                .where( [ "deliveries.created_at   < ?", data_final] ) 
                .where( [ "deliveries.status    = ?", 1 ] )  

        end

        if params[:status] == "0"  

            @deliveries = @deliveries
                .where( [ "deliveries.created_at   > ?", data_inicial] )
                .where( [ "deliveries.created_at   < ?", data_final] ) 
                .where( [ "deliveries.status  = ?", 0 ] )  

        end
        


        soma_deliveries = Delivery
            .where( [ "deliveries.seller_id    = ?", params[:seller_id] ] )     
            .where( [ "deliveries.paid_at   > ?", data_inicial] )
            .where( [ "deliveries.paid_at   < ?", data_final] )
            .where( [ "deliveries.status = ?", 1 ] )  
            .sum(:valor)  



        # despesas
        invoices_geral = Invoice
            .where( [ "invoices.seller_id    = ?", params[:seller_id] ] )  
            .where( [ "invoices.created_at   > ?", data_inicial] )
            .where( [ "invoices.created_at   < ?", data_final] )

        soma_invoices_geral = invoices_geral
            .sum(:valor) 

        soma_invoices = invoices_geral  
            .where( "invoices.nome <> 'Gasolina'")
            .where( "invoices.nome <> 'Combustivel'")
            .sum(:valor) 



        @dados = Hash.new 

        @dados[:deliveries]             = @deliveries
        @dados[:soma_invoices]          = soma_invoices
        @dados[:soma_invoices_geral]    = soma_invoices_geral 
        @dados[:soma_deliveries]        = soma_deliveries 

    end


    def date_format(date)
       date.strftime("%Y-%m-%d 00:00:00")  
    end


    def save


        @delivery = Delivery.new

        @delivery.status     = params[:deliver][:status]
        @delivery.valor      = params[:deliver][:valor]
        @delivery.latitude   = params[:deliver][:latitude]
        @delivery.longitude  = params[:deliver][:longitude]
        @delivery.client_id  = params[:deliver][:client_id]
        @delivery.obs        = params[:deliver][:obs]            
        @delivery.seller_id  = params[:seller][:id]
        @delivery.truck_id   = params[:seller][:truck_id]      


        # não coloca a data de pagamento se for a prazo
        if params[:deliver][:status].to_i == 0  
            @delivery.paid_at = nil
        else
            @delivery.paid_at = Time.new 
        end  


        if @delivery.save

            @product_delivery = ProductDelivery.new

            @product_delivery.quantidade_vendido     = params[:deliver][:quantidade_vendido]  
            @product_delivery.quantidade_bonificado  = params[:deliver][:quantidade_bonificado]    
            @product_delivery.product_id             = params[:deliver][:product_id] 
            @product_delivery.delivery_id            = @delivery.id     

           
            # reduz o numero no caminhão
            @product_delivery.save

            
            # pega o vendedor
            seller = Seller.find( params[:seller][:id] )  


            # salva estoque
            truck_prod = TruckProduct
                .where( [ "truck_products.truck_id      = ?", seller.truck_id ] )      
                .where( [ "truck_products.product_id    = ?", @product_delivery.product_id ] ) 
                .first

            truck_prod.quantidade_caminhao = 
                truck_prod.quantidade_caminhao -           
                (
                    params[:deliver][:quantidade_vendido].to_i +
                    params[:deliver][:quantidade_bonificado].to_i    
                )     

            truck_prod.save           



            # salva a localização da primeira compra do client  
            client = Client.find( params[:deliver][:client_id] )


            # nversão do amin não salva localização
            if params[:admin] 
                client.location_status  = 0        
            else
               client.location_status   = 1
               client.latitude         = params[:deliver][:latitude]
               client.longitude        = params[:deliver][:longitude]     
            end       

            client.save    


            @status            = Hash.new       
            @status[:error]    = 0

        end

    end


    def confirm_paid

        delivery = Delivery.find( params[:id] )

        delivery.status  = 1  
        delivery.paid_at = Time.new

        delivery.save   

    end


    def delete

        delivery = Delivery
            .where( [ "deliveries.id      = ?", params[:id] ] )  
            .first


        delivery.product_delivery.each do |prod_delivery|

            # pega o estoque no caminhão e devolve ao estoque
            truck_product = TruckProduct
                .where( [ "truck_products.product_id = ?",  prod_delivery.product_id ] )  
                .where( [ "truck_products.truck_id = ?",    delivery.truck_id   ] )  
                .first  


            truck_product.quantidade_caminhao = 
                truck_product.quantidade_caminhao + 
                prod_delivery.quantidade_vendido +
                prod_delivery.quantidade_bonificado

            truck_product.save

        end

        delivery.destroy

    end


end

class Api1::InvoicesController < ApplicationController

    skip_before_action :verify_authenticity_token


    def get_by_seller

        # despesas
        data_inicial = params[:data_inicial].to_datetime.strftime("%Y-%m-%d 00:00:00")
        data_final   = params[:data_final].to_datetime.strftime("%Y-%m-%d 23:59:59")  

        #
        @invoices = Invoice
            .where( [ "invoices.seller_id    = ?", params[:seller_id] ] )  
            .where( [ "invoices.created_at   > ?", data_inicial] )
            .where( [ "invoices.created_at   < ?", data_final] )
            .where(  "invoices.valor   > 0" )
            .joins(:seller)


        # outros 
        @soma_invoices = @invoices
            .where("invoices.nome <> 'Gasolina'")  
            .where("invoices.nome <> 'Combustivel'")  
            .sum(:valor)  

        # gasolina 
        @soma_invoices_gasolina = @invoices
            .where("invoices.nome = 'Combustivel'")  
            .sum(:valor)  
   

        @dados = Hash.new 

        @dados[:invoices]                 = @invoices  
        @dados[:soma_invoices]            = @soma_invoices
        @dados[:soma_invoices_gasolina]   = @soma_invoices_gasolina


    end


    def save

        @invoice = Invoice.new

        @invoice.valor          = params[:invoice][:valor]
        @invoice.nome           = params[:invoice][:nome]
        @invoice.obs            = params[:invoice][:obs]
        @invoice.seller_id      = params[:seller][:id]


        @dados = Hash.new 

        if @invoice.save
            @dados[:error_code]         =   0  
            @dados[:error_message]      =   "sucesso"
        else
            @dados[:error_code]         =   1  
            @dados[:error_message]      =   "Error ao salvar"
        end  

        puts params.inspect 

    end


end

class Api1::ClientsController < ApplicationController

    skip_before_action :verify_authenticity_token


	def index_

		@clients = Client
			.find_by_sql(
				"SELECT * FROM (SELECT *,(((acos(sin((" + params[:latitude] + "*pi()/180)) * 
				sin((latitude*pi()/180))+cos((" + params[:latitude] + "*pi()/180)) * 
				cos((latitude*pi()/180)) * cos((( " + params[:longitude] + " - longitude)* 
				pi()/180))))*180/pi())*60*1.1515 ) as distance 
				FROM clients WHERE 1 GROUP BY ID) as X 
				ORDER BY distance, nome ASC"  
           )     


	end


	def index

		@clients = Client.all.order(nome: :asc) 

	end


	def add

		@client  = Client.new

		@client.latitude    = params[:latitude]
		@client.longitude   = params[:longitude]
		@client.nome        = params[:nome]
		@client.tipo_preco  = params[:tipo_preco] 

		@client.save

	end


end

class Admin::ClientsController < Admin::ApplicationController
  

  before_action :set_admin_client, only: [:show, :edit, :update, :destroy]

  before_action :set_admin_products



  def index

    @admin_clients = Client.all 

    if params[:q]

      @admin_clients = Client
        .where( [ "clients.nome LIKE ?", "%#{params[:q]}%"  ] )
        .or( 
          Client.where( [ "clients.telefone LIKE ?", "%#{params[:q]}%"  ] ) 
        )
        .or( 
          Client.where( [ "clients.endereco LIKE ?", "%#{params[:q]}%"  ] ) 
        )
        .or( 
          Client.where( [ "clients.obs LIKE ?", "%#{params[:q]}%"  ] ) 
        )  
      
    end   


    @admin_clients      = @admin_clients.paginate( :page => params[:page] ) 
    

  end



  def show
  end


  def new
    @admin_client = Client.new
  end


  def edit  
  end


  def create

    @admin_client = Client.new(admin_client_params)


    if @admin_client.save

      params[:product].each do |product_id, preco|

        client_product = ClientProduct.new

        client_product.client_id  = @admin_client.id  
        client_product.product_id = product_id.to_i
        client_product.preco      = preco.to_f

        client_product.save  

      end

      flash[:success] = "Salvo com sucesso!"  

      redirect_to :controller => 'clients', :action => 'index'

    else

      render :new

    end 

    params.inspect

  end  


  def update

    respond_to do |format|

      if @admin_client.update(admin_client_params)

        params[:product].each do |product_id, preco|

          client_product = ClientProduct
            .where( [ "client_products.client_id = ?", @admin_client.id ] )
            .where( [ "client_products.product_id = ?", product_id.to_i ] )
            .first

          client_product.preco      = preco.to_f  

          client_product.save    

        end

        format.html { redirect_to [ :admin,  @admin_client], notice: 'Salvo com sucesso.' }

      else

        format.html { render :edit }

      end  
    end

  end


  def destroy

    @admin_client.destroy

    respond_to do |format|
      format.html { redirect_to admin_clients_url, notice: 'Excluido.' }
      format.json { head :no_content }
    end

  end

  private


    def set_admin_client
      @admin_client = Client.find(params[:id])
    end    


    def set_admin_products
      @admin_products = Product.all
    end

    def admin_client_params
      params.require(:client).permit(:nome, :tipo_preco, :telefone, :latitude, :longitude, :endereco, :obs)
    end


end
class Admin::InvoicesController < Admin::ApplicationController


  before_action :set_admin_invoice, only: [:show, :edit, :update, :destroy]
  
  before_action :set_admin_sellers, only: [:index]

  before_action :set_admin_clients, only: [:index]
  

  # GET /admin/deliveries
  # GET /admin/deliveries.json
  def index

    Delivery.per_page = 6 
    

    @data_inicial = Time.now.strftime("%Y-%m-%d")  
    @data_final   = Time.now.strftime("%Y-%m-%d")  
      
    if params[:data_inicial]
      @data_inicial = params[:data_inicial].to_datetime.strftime("%Y-%m-%d")  
    end  

    if params[:data_final]
      @data_final   = params[:data_final].to_datetime.strftime("%Y-%m-%d")  
    end


    @admin_invoices = Invoice
        .where( [ "invoices.created_at   > ?", @data_inicial + " 00:00:00"] )    
        .where( [ "invoices.created_at   < ?", @data_final + " 23:59:59"] ) 
        .order('id DESC')  


    if params[:seller_id] != '0'
      @admin_invoices =   
          @admin_invoices.where( [ "invoices.seller_id = ?", params[:seller_id] ] )
    end


    if params[:tipo_despesa] && params[:tipo_despesa] != '' 
      @admin_invoices = 
          @admin_invoices.where(  [ "invoices.nome LIKE ?", "%#{params[:tipo_despesa]}%" ] )   
    end

    # outros 
    @soma_invoices_geral = @admin_invoices
        .sum(:valor)   

    # outros 
    @soma_invoices_outros = @admin_invoices
        .where("invoices.nome <> 'Combustivel'")  
        .where("invoices.nome <> 'Gasolina'")  
        .sum(:valor)  

    # gasolina 
    @soma_invoices_gasolina = @admin_invoices
        .where("invoices.nome = 'Combustivel'")  
        .sum(:valor) 


    @admin_invoices      = @admin_invoices.paginate( :page => params[:page] ) 


  end

  # GET /admin/invoices/1
  # GET /admin/invoices/1.json
  def show
  end

  # GET /admin/invoices/new
  def new
    @admin_invoice = Invoice.new
  end

  # GET /admin/invoices/1/edit
  def edit
  end


  def create
    @admin_invoice = Invoice.new(admin_invoice_params)

    respond_to do |format|
      if @admin_invoice.save
        format.html { redirect_to @admin_invoice, notice: 'Invoice was successfully created.' }
        format.json { render :show, status: :created, location: @admin_invoice }
      else
        format.html { render :new }
        format.json { render json: @admin_invoice.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    respond_to do |format|
      if @admin_invoice.update(admin_invoice_params)
        format.html { redirect_to @admin_invoice, notice: 'Invoice was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_invoice }
      else
        format.html { render :edit }
        format.json { render json: @admin_invoice.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @admin_invoice.destroy
    respond_to do |format|
      format.html { redirect_to admin_invoices_url, notice: 'Excluido com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_invoice
      @admin_invoice = Invoice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_invoice_params
      params.require(:admin_invoice).permit(:valor, :description, :seller_id)
    end

    def set_admin_sellers
      @admin_sellers =  Seller.all    
    end

    def set_admin_clients
      @admin_clients =  Client.all
    end
    

end

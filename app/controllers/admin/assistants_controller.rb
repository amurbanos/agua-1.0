class Admin::AssistantsController < Admin::ApplicationController
  before_action :set_admin_assistant, only: [:show, :edit, :update, :destroy]



  def index
    @admin_assistants = Assistant.all
  end


  def show
  end


  def new
    @admin_assistant = Assistant.new
  end


  def edit
  end


  def create
    @admin_assistant = Assistant.new(admin_assistant_params)

      if @admin_assistant.save

        flash[:success] = "Salvo com sucesso!"

        redirect_to :controller => 'assistants', :action => 'index'

      else  

        render :new

      end  

  end


  def update
    respond_to do |format|
      if @admin_assistant.update(admin_assistant_params)

        format.html { redirect_to admin_assistants_url, notice: 'Excluido.' }  

      else
        format.html { render :edit }
      end
    end
  end


  def destroy
    @admin_assistant.destroy
    respond_to do |format|
      format.html { redirect_to admin_assistants_url, notice: 'Excluido.' }
      format.json { head :no_content }
    end
  end

  private


    def set_admin_assistant
      @admin_assistant = Assistant.find(params[:id])
    end


    def admin_assistant_params
      params.require(:assistant).permit( :nome, :telefone, :doc, :pagamento_diaria )
    end


end

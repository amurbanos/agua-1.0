class Admin::ReportsController < Admin::ApplicationController

  

  before_action :set_admin_invoice, only: [:show, :edit, :update, :destroy]
  
  before_action :set_admin_sellers, only: [:index]

  before_action :set_admin_clients, only: [:index]
  
  

  def index

    @data_inicial = Time.now.strftime("%Y-%m-%d")  
    @data_final   = Time.now.strftime("%Y-%m-%d")  


    if params[:data_inicial]
      @data_inicial = params[:data_inicial].to_datetime.strftime("%Y-%m-%d")  
    end  

    if params[:data_final]
      @data_final   = params[:data_final].to_datetime.strftime("%Y-%m-%d")  
    end

    @sellers = Seller.select(:id, :nome)

    if params[:seller_id] != '0'  
      @sellers =   
          @sellers.where( [ "sellers.id = ?", params[:seller_id] ] )
    end


  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_invoice
      @admin_invoice = Invoice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_invoice_params
      params.require(:admin_invoice).permit(:valor, :description, :seller_id)
    end

    def set_admin_sellers
      @admin_sellers =  Seller.all    
    end

    def set_admin_clients
      @admin_clients =  Client.all
    end
    

end

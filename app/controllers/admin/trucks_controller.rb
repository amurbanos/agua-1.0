class Admin::TrucksController < Admin::ApplicationController
  
  skip_before_action :verify_authenticity_token 

  before_action :set_admin_truck, only: [:show, :edit, :update, :destroy]



  def set_qtd

    # add ao caminhao
    @admin_truck_product = TruckProduct.find(params[:modalTruckProductId])

    @admin_truck_product.quantidade_caminhao =   
        @admin_truck_product.quantidade_caminhao +
        params[:modalTruckProductQtd].to_i

    @admin_truck_product.quantidade_fonte =   
        @admin_truck_product.quantidade_fonte +
        params[:modalTruckProductQtd].to_i  

    @admin_truck_product.save 

    redirect_to :controller => 'trucks', :action => 'index'  

    puts params.inspect   

  end

  def set_bottle

    @bottle            = Bottle.find(params[:modalBottleId])
    @bottle.quantidade = @bottle.quantidade + params[:modalBottleQtd].to_i
    @bottle.save
    redirect_to :controller => 'trucks', :action => 'index', anchor: "modalBottle--#{@bottle.id}"  

  end


  def clear_truck

    TruckProduct.where( [ "truck_products.truck_id = ?", params[:truck_id] ] ).each do | truck_product |

      truck_product.quantidade_fonte = 0
      
      truck_product.save  

    end

    redirect_to :controller => 'trucks', :action => 'index', anchor: "truck--#{params[:truck_id]}"     

  end 



  def index
    @admin_trucks = Truck.where( [ "trucks.status = ?", 1 ] )  
  end


  def show
    
  end


  def new
    @admin_truck = Truck.new
  end  


  def edit
  end


  def create

    @admin_truck = Truck.new(admin_truck_params)

    respond_to do |format|

      # cria produtos pro caminhao
      if @admin_truck.save

        #
        @products = Product.all

        @products.each do | product |

            truck_product = TruckProduct.new

            truck_product.product_id          = product.id  
            truck_product.truck_id            = @admin_truck.id
            truck_product.quantidade_fonte    = 0
            truck_product.quantidade_caminhao = 0

            truck_product.save   

        end

        # 
        current_year = Time.new.year
        final_year   = Time.new.year.to_i + 10

        while current_year <= (final_year)  do    

          bottle = Bottle.new

          bottle.ano        = current_year
          bottle.quantidade = 0
          bottle.truck_id   = @admin_truck.id

          bottle.save  

          current_year = current_year + 1

        end

        format.html { redirect_to :action => 'index', notice: 'Salvo com sucesso.' }

      else

        format.html { render :new }

      end
    end

  end

  # PATCH/PUT /admin/trucks/1
  # PATCH/PUT /admin/trucks/1.json
  def update
    respond_to do |format|
      if @admin_truck.update(admin_truck_params)
        format.html { redirect_to @admin_truck, notice: 'Salvo com sucesso.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /admin/trucks/1
  # DELETE /admin/trucks/1.json
  def destroy
    @admin_truck.destroy
    respond_to do |format|
      format.html { redirect_to admin_trucks_url, notice: 'Excluido com sucesso.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_truck
      @admin_truck = Truck.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_truck_params
      params.require(:truck).permit(:placa, :obs)
    end


    def save_products( truck_id )

    end

end

class Admin::ProductsController < Admin::ApplicationController
  
  before_action :set_admin_product, only: [:show, :edit, :update, :destroy]

  before_action :set_admin_brands

  before_action :set_admin_sellers


  def index
    @admin_products = Product.all
  end


  def show
  end


  def new
    @admin_product = Product.new
  end


  def edit
  end


  def create

    @admin_product = Product.new(admin_product_params)


    if @admin_product.save

      # adicionar esse produto
      @clients = Client.all

      @clients.each do |client|

        client_product = ClientProduct.new

        client_product.client_id  = client.id
        client_product.product_id = @admin_product.id   
        client_product.product_id = @admin_product.id     

        client_product.save

      end

      flash[:success] = "Salvo com sucesso!"

      redirect_to :controller => 'products', :action => 'index'

    else
      render :new
    end

  end


  def update

      if @admin_product.update(admin_product_params)

        # salva os vendedores desse produto
        ProductSeller
          .where(:product_id => @admin_product.id)
          .destroy_all


        if params[:seller_id]  
          params[:seller_id].each do |seller_id|

            product_seller = ProductSeller.new
            product_seller.product_id = @admin_product.id
            product_seller.seller_id  = seller_id 
            product_seller.save  

          end  
        end  

        # Confirma e redireciona  
        flash[:success] = "Salvo com sucesso!"
        redirect_to :controller => 'products', :action => 'index'

      else
        render :edit
      end

  end


  def destroy
    #@admin_product.destroy
    #respond_to do |format|
    #  format.html { redirect_to admin_products_url, notice: 'Excluido com sucesso.' }
    #  format.json { head :no_content }
    #end
  end


  private

    def set_admin_product
      @admin_product = Product.find(params[:id])
    end

    def admin_product_params
      params.require(:product).permit(
        :nome, 
        :obs,  
        :preco_base,   
        :preco_compra_1,   
        :preco_compra_2,     
        :preco_venda_1,  
        :preco_venda_2,  
        :brand_id,     
      )
    end
 
    def set_admin_brands
      @admin_brands = Brand.all 
    end

    def set_admin_sellers
      @admin_sellers = Seller.all 
    end


end

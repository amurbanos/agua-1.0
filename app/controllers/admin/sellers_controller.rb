class Admin::SellersController < Admin::ApplicationController

  before_action :set_admin_seller, only: [:show, :edit, :update, :destroy]


  def index
    @admin_sellers = Seller.all
  end


  def show
  end


  def new
    @admin_seller = Seller.new
  end


  def edit
  end


  def create
    @admin_seller = Seller.new(admin_seller_params)

    if @admin_seller.save

      flash[:success] = "Salvo com sucesso!"

      redirect_to :controller => 'sellers', :action => 'index'

    else

       render :new    

    end

  end


  def update

      if @admin_seller.update(admin_seller_params)

        flash[:success] = "Salvo com sucesso!"

        redirect_to :controller => 'sellers', :action => 'index'  

      else
         render :edit 
      end


  end


  def destroy

    @admin_seller.destroy

    respond_to do |format|
      format.html { redirect_to admin_sellers_url, notice: 'Excluido com sucesso.' }
    end
    
  end

  private


    def set_admin_seller
      @admin_seller = Seller.find(params[:id])
    end


    def admin_seller_params
      params.require(:seller).permit(
        :nome, :usuario, :senha, :telefone, :pagamento_diaria, :tipo_vendedor, :documento, :obs, :ativo, :imei  
      )
    end


end

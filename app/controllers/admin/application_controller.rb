class Admin::ApplicationController < ApplicationController  #ActionController::Base

     before_action :authenticate_user!

     before_action :set_general_dates
     before_action :set_general_counts


	 layout 'admin/admin'


	 def set_general_dates  

	 	@general_hoje              	= Date.today 

	 	@general_mes_ultimo        	= Date.today.end_of_month

	 	@general_semana_primeiro   	= Date.today.beginning_of_week
	 	@general_semana_ultimo  	= Date.today.end_of_week

	 	@general_semana_anterior_primeiro   = @general_semana_primeiro - 7.days
	 	@general_semana_anterior_ultimo		= @general_semana_ultimo  - 7.days

     end



	 def set_general_counts

	 	@count_products = Product.all.count 
	 	@count_brands   = Brand.all.count 

     end


end

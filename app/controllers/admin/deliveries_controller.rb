class Admin::DeliveriesController < Admin::ApplicationController


  before_action :set_admin_delivery, only: [:show, :edit, :update, :destroy]

  before_action :set_admin_sellers, only: [:index]

  before_action :set_admin_clients, only: [:index]

  before_action :set_admin_trucks, only: [:index]


  def sign
  end


  # GET /admin/deliveries
  # GET /admin/deliveries.json
  def index

    Delivery.per_page = 6


    @data_inicial = Time.now.strftime("%Y-%m-%d")  
    @data_final   = Time.now.strftime("%Y-%m-%d")  

      
    if params[:data_inicial]
      @data_inicial = params[:data_inicial].to_datetime.strftime("%Y-%m-%d")  
    end

    if params[:data_final]
      @data_final   = params[:data_final].to_datetime.strftime("%Y-%m-%d")  
    end    
       

    @admin_deliveries = Delivery.order('id DESC')  

    if params[:status] != ""  
      @admin_deliveries = @admin_deliveries
          .where( [ "deliveries.status   = ?", params[:status] ] )       
    end    

    if params[:seller_id] != '0'  
      @admin_deliveries = 
          @admin_deliveries.where( [ "deliveries.seller_id = ?", params[:seller_id] ] )
    end

    if params[:client_id] != '0'  
      @admin_deliveries = 
          @admin_deliveries.where( [ "deliveries.client_id = ?", params[:client_id] ] )
    end

    if params[:truck_id] != '0'  
      @admin_deliveries = 
          @admin_deliveries.where( [ "deliveries.truck_id = ?", params[:truck_id] ] )
    end


    #vendas recebido 
    @soma_admin_deliveries_recebido = @admin_deliveries
      .where( [ "deliveries.status = ?", 1 ] )  
      .where( [ "deliveries.paid_at   > ?", @data_inicial + " 00:00:00"] )    
      .where( [ "deliveries.paid_at   < ?", @data_final + " 23:59:59"] ) 
      .sum( :valor ) 


    @admin_deliveries = @admin_deliveries
        .where( [ "deliveries.created_at   > ?", @data_inicial + " 00:00:00"] )    
        .where( [ "deliveries.created_at   < ?", @data_final + " 23:59:59"] ) 


    # vendas geral
    @soma_admin_deliveries = @admin_deliveries.sum( :valor ) 


    #vendas pendente 
    @soma_admin_deliveries_pendente = @admin_deliveries  
      .where( [ "deliveries.status = ?", 0 ] )
      .sum( :valor ) 


    # soma despesas
    deliveries_seller_ids = []


    @admin_deliveries.each do |delivery|
      deliveries_seller_ids.push( delivery.seller_id )
    end

    # Soma das faturas
    @admin_invoices = Invoice
        .where( [ "invoices.created_at   > ?", @data_inicial + " 00:00:00"] )    
        .where( [ "invoices.created_at   < ?", @data_final + " 23:59:59"] ) 
        .where("invoices.seller_id IN (?)", deliveries_seller_ids)    
        .order('id DESC')      

    if params[:seller_id] != '0'  
      @admin_invoices = 
          @admin_invoices.where( [ "invoices.seller_id = ?", params[:seller_id] ] )
    end


    @soma_admin_invoices_combustivel = @admin_invoices
      .where( 
        [ 
          "invoices.nome = ?", 'Combustivel'
        ] 
      )
      .sum( :valor ) 


    @soma_admin_invoices = @admin_invoices
      .where("invoices.nome <> 'Combustivel'")    
      .where("invoices.nome <> 'Gasolina'")  
      .sum( :valor ) 

    @soma_admin_invoices_geral = @admin_invoices
      .sum( :valor ) 


    if params[:status] == '0'  
      @soma_admin_invoices = 0     
    end    

    # paginar
    @admin_deliveries      = @admin_deliveries.paginate( :page => params[:page] )


  end


  def show
  end


  def new
    @admin_delivery = Delivery.new
  end


  def edit
  end


  def create
    @admin_delivery = Delivery.new(admin_delivery_params)
   
    respond_to do |format|
      if @admin_delivery.save
        format.html { redirect_to @admin_delivery, notice: 'Delivery was successfully created.' }
        format.json { render :show, status: :created, location: @admin_delivery }
      else
        format.html { render :new }
        format.json { render json: @admin_delivery.errors, status: :unprocessable_entity }
      end
    end
  end



  def update
    respond_to do |format|
      if @admin_delivery.update(admin_delivery_params)
        format.html { redirect_to @admin_delivery, notice: 'Delivery was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_delivery }
      else
        format.html { render :edit }
        format.json { render json: @admin_delivery.errors, status: :unprocessable_entity }
      end
    end
  end



  def cancel_delivery

    @admin_delivery = Delivery.find( params[:id] )    

    @admin_delivery.destroy_complete  

    respond_to do |format|
      format.html { redirect_to admin_deliveries_url, notice: 'Excluido com sucesso.' }
    end

  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_delivery
      @admin_delivery = Delivery.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_delivery_params
      params.require(:admin_delivery).permit(:valor, :latitude, :longitude, :obs, :seller_id, :client_id)
    end

    def set_admin_sellers
      @admin_sellers =  Seller.all.order(nome: :asc)  
    end

    def set_admin_clients
      @admin_clients =  Client.all.order(nome: :asc)
    end

    def set_admin_trucks
      @admin_trucks =  Truck.all.order(placa: :asc)
    end


end

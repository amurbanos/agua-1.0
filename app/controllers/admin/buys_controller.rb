class Admin::BuysController < Admin::ApplicationController

  before_action :set_admin_buy, only: [:show, :edit, :update]  

  before_action :set_admin_sellers, only: [:index]

  before_action :set_admin_clients, only: [:index]

  before_action :set_admin_products, only: [:index]

  before_action :set_admin_trucks, only: [:index]

  before_action :set_admin_brands, only: [:index]


  def index


    @data_inicial = Time.now.strftime("%Y-%m-%d")  
    @data_final   = Time.now.strftime("%Y-%m-%d")     

      
    if params[:data_inicial]
      @data_inicial = params[:data_inicial].to_datetime.strftime("%Y-%m-%d")  
    end

    if params[:data_final]
      @data_final   = params[:data_final].to_datetime.strftime("%Y-%m-%d")  
    end  

    @admin_buys = Buy
        .where( [ "buys.created_at   > ?", @data_inicial + " 00:00:00"] )    
        .where( [ "buys.created_at   < ?", @data_final + " 23:59:59"] ) 
        .order('id DESC') 


    if params[:seller_id] != '0'  
      @admin_buys =  @admin_buys.where( [ "buys.seller_id = ?", params[:seller_id] ] )
    end

    if params[:truck_id] != '0'  
      @admin_buys = @admin_buys.where( [ "buys.truck_id = ?", params[:truck_id] ] )
    end

    if params[:product_id] != '0'  
      @admin_buys = @admin_buys.where( [ "buys.product_id = ?", params[:product_id] ] )
    end 

    if params[:brand_id] != '0'  
      @admin_buys = @admin_buys
          .joins(:product)  
          .where( [ "products.brand_id = ?", params[:brand_id] ] )  
    end 

    # Compras geral
    @soma_buys = @admin_buys.sum( :valor ) 

    # Compras geral
    @soma_qtd = @admin_buys.sum( :quantidade ) 


  end


  def show
  end


  def new
    @admin_buy = Buy.new
  end


  def edit
  end


  def create

    @admin_buy = Buy.new(admin_buy_params)

    respond_to do |format|
      if @admin_buy.save
        format.html { redirect_to @admin_buy, notice: 'Buy was successfully created.' }
        format.json { render :show, status: :created, location: @admin_buy }
      else
        format.html { render :new }
        format.json { render json: @admin_buy.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    respond_to do |format|
      if @admin_buy.update(admin_buy_params)
        format.html { redirect_to @admin_buy, notice: 'Buy was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_buy }
      else
        format.html { render :edit }
        format.json { render json: @admin_buy.errors, status: :unprocessable_entity }
      end
    end
  end


  def cancel_buy

    @admin_buy = Buy.find( params[:buy_id] )    
    

    @admin_buy.destroy_complete


    respond_to do |format|
      format.html { redirect_to admin_buys_url, notice: 'Compra cancelada.' }  
    end    
    

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_buy
      @admin_buy = Admin::Buy.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_buy_params
      params.require(:admin_buy).permit(:quantidade, :valor, :valor, :obs, :truck_id, :seller_id, :product_id)
    end

    def set_admin_sellers
      @admin_sellers =  Seller.all    
    end

    def set_admin_clients
      @admin_clients =  Client.all
    end

    def set_admin_products
      @admin_products =  Product.all
    end

    def set_admin_trucks
      @admin_trucks =  Truck.all
    end

    def set_admin_brands
      @admin_brands =  Brand.all
    end

end

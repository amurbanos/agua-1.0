class Admin::PointsController < Admin::ApplicationController


  before_action :set_admin_point, only: [:show, :edit, :update, :destroy]


  before_action :set_admin_sellers, only: [:index]

  before_action :set_admin_clients, only: [:index]

  before_action :set_admin_products, only: [:index]

  before_action :set_admin_trucks, only: [:index]

  before_action :set_admin_assistants, only: [:index]


  def index

    @data_inicial = Time.now.strftime("%Y-%m-%d")  
    @data_final   = Time.now.strftime("%Y-%m-%d")     

      
    if params[:data_inicial]
      @data_inicial = params[:data_inicial].to_datetime.strftime("%Y-%m-%d")  
    end

    if params[:data_final]
      @data_final   = params[:data_final].to_datetime.strftime("%Y-%m-%d")  
    end  


    @admin_points = Point
        .where( [ "points.created_at   > ?", @data_inicial + " 00:00:00"] )    
        .where( [ "points.created_at   < ?", @data_final + " 23:59:59"] ) 
        .order('id DESC') 



    if params[:func_id]

      if params[:func_id].include? "vendedor--"
        @admin_points = Point
            .where( [ "points.seller_id  = ?",  params[:func_id].split("--").last ] )    
      end   

      if params[:func_id].include? "ajudante--"
        @admin_points = Point
            .where( [ "points.assistant_id  = ?",  params[:func_id].split("--").last ] )    
      end   

    end

    # 
    @soma_pagamento_diaria = @admin_points.sum( :pagamento_diaria )        
 

    # 
    @soma_pagamento_diaria = @admin_points.sum( :pagamento_diaria ) 

    # paginar
    @admin_points      = @admin_points.paginate( :page => params[:page] ) 

    
      
  end


  private

    def set_admin_point
      @admin_point = Point.find(params[:id])
    end


    def admin_point_params
      params.require(:admin_point).permit(:seller_id, :assistant_id)
    end

    def set_admin_sellers
      @admin_sellers =  Seller.all    
    end

    def set_admin_clients
      @admin_clients =  Client.all
    end

    def set_admin_products
      @admin_products =  Product.all
    end

    def set_admin_trucks
      @admin_trucks =  Truck.all
    end   

    def set_admin_assistants
      @admin_assistants =  Assistant.all
    end    

end

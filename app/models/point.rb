class Point < ApplicationRecord

    belongs_to :seller,  optional: true  
    belongs_to :assistant,  optional: true  


    def self.beat_today(seller_id, assistant_id)

        # ponto do vendedor
        ponto_vendedor = 
            self
              .where(
                seller_id: seller_id
              )
              .where(
                created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day
              )
              .first

        if ponto_vendedor == nil

            seller = Seller.find( seller_id )  

            ponto_vendedor = Point.new
            ponto_vendedor.seller_id = seller_id
            ponto_vendedor.pagamento_diaria = seller.pagamento_diaria     
            ponto_vendedor.save

        end


        # ponto do vendedor
        ponto_ajudante = 
            self
              .where(
                assistant_id: assistant_id
              )
              .where(
                created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day
              )
              .first

        if ponto_ajudante == nil

            assistant = Assistant.find( assistant_id )

            ponto_ajudante = Point.new  
            ponto_ajudante.assistant_id = assistant_id 
            ponto_ajudante.pagamento_diaria = assistant.pagamento_diaria     
            ponto_ajudante.save  

        end

      

    end


end  

class Buy < ApplicationRecord

  belongs_to :truck
  belongs_to :seller
  belongs_to :product 


  def destroy_complete  
    
  	truck_product = TruckProduct
        .where( [ "truck_products.truck_id     = ?", self.truck_id] )    
        .where( [ "truck_products.product_id   = ?", self.product_id] ) 
        .first  

    truck_product.quantidade_fonte    -=  self.quantidade
    truck_product.quantidade_caminhao -=  self.quantidade  

    if truck_product.save

      self.destroy
          
    end    

  end



end

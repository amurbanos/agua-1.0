class Seller < ApplicationRecord

	 attr_accessor :message

	 belongs_to :truck,  optional: true  

	 has_many :product_seller
	 has_many :delivery  
	 has_many :invoice
	 has_many :point
	 has_many :buy


	def sum_invoices(data_inicial, data_final, tipo = 0)

	  tmp_i = self.invoice

	  tmp_i = tmp_i
        .where( [ "invoices.created_at   > ?", data_inicial + " 00:00:00"] )    
        .where( [ "invoices.created_at   < ?", data_final + " 23:59:59"] ) 

      if tipo != 0
		tmp_i = tmp_i
          .where(  [ "invoices.nome LIKE ?", "%#{tipo}%" ] )     
      end

	  return tmp_i.sum(:valor) 

	end



	def sum_deliveries(data_inicial, data_final)

	  tmp_d = self.delivery

	  tmp_d = tmp_d
        .where( [ "deliveries.created_at   > ?", data_inicial + " 00:00:00"] )    
        .where( [ "deliveries.created_at   < ?", data_final + " 23:59:59"] ) 

	  return tmp_d.sum(:valor) 

	end
 

	def sum_points(data_inicial, data_final)

	  tmp_points = self.point

	  tmp_points = tmp_points
        .where( [ "points.created_at   > ?", data_inicial + " 00:00:00"] )      
        .where( [ "points.created_at   < ?", data_final + " 23:59:59"] ) 

	  return tmp_points.sum(:pagamento_diaria)   

	end

	def sum_buys(data_inicial, data_final)

	  tmp_buys = self.buy

	  tmp_buys = tmp_buys
        .where( [ "buys.created_at   > ?", data_inicial + " 00:00:00"] )        
        .where( [ "buys.created_at   < ?", data_final + " 23:59:59"] ) 

	  return tmp_buys.sum(:valor)   

	end


	def qtd_entregas(data_inicial, data_final)

	  tmp_d = self.delivery

	  tmp_d = tmp_d
	    .select( :id )
        .where( [ "deliveries.created_at   > ?", data_inicial + " 00:00:00"] )    
        .where( [ "deliveries.created_at   < ?", data_final + " 23:59:59"] ) 

      tmp_product_deliveries = ProductDelivery.where("delivery_id IN (?)", tmp_d )

	  return tmp_product_deliveries.sum(:quantidade_vendido) +  
	    tmp_product_deliveries.sum(:quantidade_bonificado)  

	end




end

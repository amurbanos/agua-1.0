class Delivery < ApplicationRecord
  

    belongs_to :seller
    belongs_to :truck
    belongs_to :client
    has_many   :product_delivery #, :dependent => :destroy  

    before_save :set_distance_to_client

    # before_destroy :destroy_relation


    def destroy_complete


        product_delivery =  ProductDelivery
            .where( [ "product_deliveries.delivery_id = ?", self.id] )    
            .first  


        truck_product = TruckProduct
            .where( [ "truck_products.truck_id     = ?", self.truck_id ] )    
            .where( [ "truck_products.product_id   = ?", product_delivery.product_id ] )     
            .first  


        truck_product.quantidade_caminhao +=  ( 
            product_delivery.quantidade_vendido + 
            product_delivery.quantidade_bonificado 
        )

        truck_product.save        

        product_delivery.destroy

        self.destroy

    end


    def set_distance_to_client

    	client = Client.find( self.client_id )

    	distance = self.distance_between(
    		self.latitude,
    		self.longitude,
            client.latitude,
    		client.longitude 
    	) 

        self.distancia = distance.to_i

        puts "==================================================================="
        puts distance.to_i.inspect

    end


    def set_location

        

    end


	def distance_between(lat1, lon1, lat2, lon2)  

		rad_per_deg = Math::PI / 180
		rm = 6371000 # Earth radius in meters


		lat1_rad, lat2_rad = lat1 * rad_per_deg, lat2 * rad_per_deg  
        lon1_rad, lon2_rad = lon1 * rad_per_deg, lon2 * rad_per_deg

	    a = Math.sin((lat2_rad - lat1_rad) / 2) ** 2 + Math.cos(lat1_rad) * 
	        Math.cos(lat2_rad) * Math.sin((lon2_rad - lon1_rad) / 2) ** 2  

	    c = 2 * Math::atan2(Math::sqrt(a), Math::sqrt(1 - a))

	    rm * c

	end


end

class Truck < ApplicationRecord

    has_many :truck_product

    has_many :location
    
    has_many :bottle, -> { 
        where 'ano >= ' +  Time.new.year.to_s 
        where 'ano <= ' +  ( Time.new.year + 3 ).to_s 
    }


    validates_uniqueness_of :placa, :message => " - Placa já cadastrada"  


    def fonte_week ( product_id ) 
        total = Buy
            .where( [ "buys.product_id  = ?", product_id ] )     
            .where( [ "buys.truck_id    = ?", self.id ] )  
            .where( [ "buys.created_at   > ?", Date.today.beginning_of_week.beginning_of_day] )
            .where( [ "buys.created_at   < ?", Date.today.end_of_week.end_of_day] )
            .sum(:quantidade)  

        return total
    end


    def fonte_day ( product_id ) 
        total = Buy
            .where( [ "buys.product_id  = ?", product_id ] )     
            .where( [ "buys.truck_id    = ?", self.id ] )  
            .where( [ "buys.created_at   > ?", Date.today.beginning_of_day] )
            .where( [ "buys.created_at   < ?", Date.today.end_of_day] )  
            .sum(:quantidade)  

        return total
    end


    def sell_week ( product_id )  
        total = ProductDelivery
            .where( [ "product_deliveries.product_id  = ?", product_id ] )       
            .joins(:delivery)  
            .where( [ "deliveries.created_at   > ?", Date.today.beginning_of_week.beginning_of_day] )
            .where( [ "deliveries.created_at   < ?", Date.today.end_of_week.end_of_day] )
            .where( [ "deliveries.truck_id   = ?", self.id] )

        return total.sum( :quantidade_vendido )
    end


    def sell_day ( product_id )  
        total = ProductDelivery
            .where( [ "product_deliveries.product_id  = ?", product_id ] )       
            .joins(:delivery)  
            .where( [ "deliveries.created_at   > ?", Date.today.beginning_of_day] )
            .where( [ "deliveries.created_at   < ?", Date.today.end_of_day] )  
            .where( [ "deliveries.truck_id   = ?", self.id] )
        return total.sum( :quantidade_vendido )
    end


end
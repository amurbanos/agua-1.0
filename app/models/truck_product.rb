class TruckProduct < ApplicationRecord

  belongs_to :truck
  belongs_to :product

  before_save :set_min_zero

  def set_min_zero

  	if self.quantidade_fonte < 0
    	self.quantidade_fonte = 0
  	end

  	if self.quantidade_caminhao < 0
    	self.quantidade_caminhao = 0
  	end

  end


end
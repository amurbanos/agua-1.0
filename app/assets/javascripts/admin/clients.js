var admin_clients;


$( document ).ready(function() {



    function geocode(platform, address) 
    {
    
        var geocoder = platform.getGeocodingService(),

        geocodingParameters = {
          searchText: address,
          jsonattributes : 1
        };

        geocoder.geocode(
            geocodingParameters,     
            onSuccess,
            onError
        );

    }

    function onSuccess(result) 
    {

        if (typeof result.response !== 'undefined') {

            var locations = result.response.view[0].result; 


            // $("#client_longitude").val( locations[0].location.navigationPosition[0].longitude );  
            // $("#client_latitude").val( locations[0].location.navigationPosition[0].latitude );  

            $("#client_sugest").html( locations[0].location.address.label ); 

            $("#client_sugest_container").fadeIn();    

            console.log( locations ); 

        }


 

    }


    function onError(error) {
      alert('Ooops!');
    }


    function delay(callback, ms) {
      var timer = 0;
      return function() {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
          callback.apply(context, args);
        }, ms || 0);
      };
    }

    //
    admin_clients = {

        /*
        *
        */
        getCoordByAddress : function( endereco ){


            var platform = new H.service.Platform({  
                'app_id': 'xLCFXVMRONHZDZ1BCcds',
                'app_code': 'et-o6Pb5bNjJxObiALOTpA'
            });


            geocode( platform, endereco); 
           
        },


    };

    $('#client_endereco').keyup(delay(function (e) {
    
        admin_clients.getCoordByAddress( $(this).val() );

    }, 2000));



});
Rails.application.routes.draw do


  root to: 'admin/deliveries#index'   


  namespace :admin do
    resources :points
  end
  namespace :admin do
    resources :assistants
  end
  namespace :admin do
    resources :buys
  end
  devise_for :users


  #
  namespace :admin do

    root 'deliveries#index'

    resources :deliveries    
    resources :sellers  
    resources :products  
    resources :clients
    resources :invoices
    resources :trucks
    resources :reports 

    post '/trucks/set_qtd',            to: 'trucks#set_qtd'      
    post '/trucks/set_bottle',         to: 'trucks#set_bottle'        
    get  '/truck/clear_truck',         to: 'trucks#clear_truck'        

    get  '/buy/cancel_buy',            to: 'buys#cancel_buy'          

    get  '/delivery/cancel_delivery',  to: 'deliveries#cancel_delivery'          


  end  

  #
  namespace :api1 do

    # deliveries
    post '/deliveries/sign',          to: 'deliveries#sign'  
    post '/deliveries/save',          to: 'deliveries#save' 
    get  '/deliveries/get_by_seller', to: 'deliveries#get_by_seller'
    get  '/deliveries/delete',        to: 'deliveries#delete' 
    get  '/deliveries/confirm_paid',  to: 'deliveries#confirm_paid'    

    # invoices
    post '/invoices/save',         to: 'invoices#save'    
    get '/invoices/get_by_seller', to: 'invoices#get_by_seller'

    # products
    get '/products', to: 'products#index'

    # clients
    get  '/clients',           to: 'clients#index'
    post '/clients/add',           to: 'clients#add' 

    # assistants
    get  '/assistants',           to: 'assistants#index'

    # sellers
    post '/sellers/set_truck', to: 'sellers#set_truck'    
    post '/sellers/set_location', to: 'sellers#set_location'    

    # trucks  
    get '/trucks', to: 'trucks#index'          


  end



end

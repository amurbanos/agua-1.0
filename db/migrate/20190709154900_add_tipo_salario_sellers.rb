class AddTipoSalarioSellers < ActiveRecord::Migration[5.2]


  def change

    change_table(:sellers) do |t|

      t.integer :tipo_vendedor, 
          limit: 1,  
          comment: '1-Fixo, 2-diaria, 3-Autonomo',   
          default: 2

    end

  end


end
class CreateProductSellers < ActiveRecord::Migration[5.2]


  def change

    create_table :product_sellers do |t|

      t.references :product, foreign_key: true
      t.references :seller, foreign_key: true

      t.timestamps
      
    end

  end


end

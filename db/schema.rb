# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_09_172307) do

  create_table "assistants", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "nome"
    t.string "telefone"
    t.string "doc"
    t.decimal "pagamento_diaria", precision: 8, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bottles", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "truck_id"
    t.integer "quantidade"
    t.integer "ano"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["truck_id"], name: "index_bottles_on_truck_id"
  end

  create_table "brands", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "nome"
    t.text "obs"
    t.integer "dia_desconto", limit: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "buys", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "quantidade"
    t.decimal "valor", precision: 8, scale: 2
    t.text "obs"
    t.bigint "truck_id"
    t.bigint "seller_id"
    t.bigint "product_id"
    t.date "get_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_buys_on_product_id"
    t.index ["seller_id"], name: "index_buys_on_seller_id"
    t.index ["truck_id"], name: "index_buys_on_truck_id"
  end

  create_table "client_products", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "client_id"
    t.bigint "product_id"
    t.decimal "preco", precision: 8, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_client_products_on_client_id"
    t.index ["product_id"], name: "index_client_products_on_product_id"
  end

  create_table "clients", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "nome"
    t.string "telefone"
    t.integer "location_status", default: 0, null: false
    t.decimal "latitude", precision: 10, scale: 6, default: "0.0"
    t.decimal "longitude", precision: 10, scale: 6, default: "0.0"
    t.text "endereco"
    t.text "obs"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deliveries", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.decimal "valor", precision: 8, scale: 2
    t.integer "status", limit: 1
    t.integer "distancia"
    t.text "obs"
    t.decimal "latitude", precision: 10, scale: 6
    t.decimal "longitude", precision: 10, scale: 6
    t.bigint "seller_id"
    t.bigint "client_id"
    t.bigint "truck_id"
    t.datetime "paid_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_deliveries_on_client_id"
    t.index ["seller_id"], name: "index_deliveries_on_seller_id"
    t.index ["truck_id"], name: "index_deliveries_on_truck_id"
  end

  create_table "invoices", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.decimal "valor", precision: 8, scale: 2
    t.text "nome"
    t.text "obs"
    t.bigint "seller_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["seller_id"], name: "index_invoices_on_seller_id"
  end

  create_table "locations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.decimal "latitude", precision: 10, scale: 6
    t.decimal "longitude", precision: 10, scale: 6
    t.bigint "seller_id"
    t.bigint "truck_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["seller_id"], name: "index_locations_on_seller_id"
    t.index ["truck_id"], name: "index_locations_on_truck_id"
  end

  create_table "points", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "seller_id"
    t.bigint "assistant_id"
    t.decimal "pagamento_diaria", precision: 8, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["assistant_id"], name: "index_points_on_assistant_id"
    t.index ["seller_id"], name: "index_points_on_seller_id"
  end

  create_table "product_deliveries", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "quantidade_vendido"
    t.integer "quantidade_bonificado"
    t.bigint "product_id"
    t.bigint "delivery_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["delivery_id"], name: "index_product_deliveries_on_delivery_id"
    t.index ["product_id"], name: "index_product_deliveries_on_product_id"
  end

  create_table "product_sellers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.bigint "product_id"
    t.bigint "seller_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_product_sellers_on_product_id"
    t.index ["seller_id"], name: "index_product_sellers_on_seller_id"
  end

  create_table "products", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "nome"
    t.text "obs"
    t.decimal "preco_compra_1", precision: 8, scale: 2
    t.decimal "preco_compra_2", precision: 8, scale: 2
    t.decimal "preco_base", precision: 8, scale: 2
    t.bigint "brand_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["brand_id"], name: "index_products_on_brand_id"
  end

  create_table "sellers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "usuario"
    t.string "senha"
    t.decimal "pagamento_diaria", precision: 8, scale: 2, default: "0.0", null: false
    t.string "nome"
    t.string "telefone"
    t.string "documento"
    t.text "obs"
    t.integer "ativo", limit: 1
    t.string "imei"
    t.integer "truck_id", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "tipo_vendedor", limit: 1, default: 2, comment: "1-Fixo, 2-diaria, 3-Autonomo"
  end

  create_table "truck_products", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "truck_id"
    t.bigint "product_id"
    t.integer "quantidade_fonte"
    t.integer "quantidade_caminhao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_truck_products_on_product_id"
    t.index ["truck_id"], name: "index_truck_products_on_truck_id"
  end

  create_table "trucks", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "status", limit: 1, default: 1, null: false
    t.string "placa"
    t.text "obs"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "nivel"
    t.string "nome"
    t.string "telefone"
    t.string "senha"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "bottles", "trucks"
  add_foreign_key "buys", "products"
  add_foreign_key "buys", "sellers"
  add_foreign_key "buys", "trucks"
  add_foreign_key "client_products", "clients"
  add_foreign_key "client_products", "products"
  add_foreign_key "deliveries", "clients"
  add_foreign_key "deliveries", "sellers"
  add_foreign_key "deliveries", "trucks"
  add_foreign_key "invoices", "sellers"
  add_foreign_key "locations", "sellers"
  add_foreign_key "locations", "trucks"
  add_foreign_key "points", "assistants"
  add_foreign_key "points", "sellers"
  add_foreign_key "product_deliveries", "deliveries"
  add_foreign_key "product_deliveries", "products"
  add_foreign_key "product_sellers", "products"
  add_foreign_key "product_sellers", "sellers"
  add_foreign_key "products", "brands"
  add_foreign_key "truck_products", "products"
  add_foreign_key "truck_products", "trucks"
end
